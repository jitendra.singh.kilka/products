## Requirements
 - PHP >= 7.2.5
 - BCMath PHP Extension
 - Ctype PHP Extension
 - Fileinfo PHP extension
 - JSON PHP Extension
 - Mbstring PHP Extension
 - OpenSSL PHP Extension
 - PDO PHP Extension
 - mysql PHP Extension
 - Tokenizer PHP Extension
 - XML PHP Extension
 - Mysql Server

## Intallation
 ```composer install --optimize-autoloader --no-dev```

 ```set database config variable in .env```

 ```php artisan key:generate```

 ```php artisan config:cache```

 ```php artisan migrate```

  ```php artisan db:seed```

  ```php artisan serve```

## Deployed Server Address
[Server - http://15.206.75.100](http://15.206.75.100)
