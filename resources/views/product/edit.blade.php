@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('sub_category.index') }}" title="Go back"> <i
                        class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('product.update', $result['product']->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $result['product']->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Sub Category:</strong>
                    <select class="form-control" name="sub_category_id">
                        @foreach ($result['sub_categories'] as $i => $sub_category)
                        <option value="{{ $sub_category->id }}"
                        @if ($result['product']->sub_category_id == $sub_category->id)
                            selected="selected"
                        @endif
                        >{{$sub_category->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Measurement Unit:</strong>
                    <select class="form-control" name="measurement_unit_id">
                        @foreach ($result['measurement_units'] as $i => $measurement_unit)
                        <option value="{{ $measurement_unit->id }}"
                        @if ($result['product']->measurement_unit_id == $measurement_unit->id)
                            selected="selected"
                        @endif
                        >{{$measurement_unit->name .' | '.$measurement_unit->measurement_type->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Code:</strong>
                    <input type="text" name="code" class="form-control" placeholder="{{ $result['product']->code }}"
                        value="{{ $result['product']->code }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    <input type="text" name="description" class="form-control" placeholder="{{ $result['product']->description }}"
                        value="{{ $result['product']->description }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tags:</strong>
                    <input type="text" name="tags" class="form-control" placeholder="{{ $result['product']->tags }}"
                        value="{{ $result['product']->tags }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>HSN Code:</strong>
                    <input type="text" name="hsn_code" class="form-control" placeholder="{{ $result['product']->hsn_code }}"
                        value="{{ $result['product']->hsn_code }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection
