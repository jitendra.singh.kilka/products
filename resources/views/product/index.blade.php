@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Products</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('product.create') }}" title="Create a Product"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Category</th>
            <th>Type</th>
            <th>Sub Category</th>
            <th>Code</th>
            <th>Measurement Unit</th>
            <th>Is Active</th>
            <th>Description</th>
            <th>Tags</th>
            <th>HSN Code</th>
            <th>Created At</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($products as $product)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->sub_category->category->name }}</td>
                <td>{{ $product->sub_category->category->type }}</td>
                <td>{{ $product->sub_category->name }}</td>
                <td>{{ $product->code }}</td>
                <td>{{ $product->measurement_unit->name.' | '.$product->measurement_unit->measurement_type->name }}</td>
                <td>{{ $product->is_active }}</td>
                <td>{{ $product->description??'' }}</td>
                <td>{{ $product->tags??'' }}</td>
                <td>{{ $product->hsn_code??'' }}</td>
                <td>{{ $product->created_at->format('jS M Y') }}</td>
                <td>
                    <form action="{{ route('product.destroy', $product->id) }}" method="POST">

                        <a href="{{ route('product.show', $product->id) }}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>

                        <a href="{{ route('product.edit', $product->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>

                        </a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>

                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $products->links() !!}

@endsection
