@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Sub Categories</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('sub_category.create') }}" title="Create a Sub Category"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th align="center">No</th>
            <th>Name</th>
            <th>Category</th>
            <th>Code</th>
            <th>Is Active</th>
            <th>Created At</th>
            <th>Products of sub-category</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($sub_categories as $sub_category)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $sub_category->name }}</td>
                <td>{{ $sub_category->category->name }}</td>
                <td>{{ $sub_category->code }}</td>
                <td>{{ $sub_category->is_active }}</td>
                <td>{{ $sub_category->created_at->format('jS M Y') }}</td>
                <td align="center">
                    <a href="{{ url('sub_category/products/'.$sub_category->id) }}" title="show">
                        <i class="fas fa-align-justify  fa-lg"></i>
                    </a>
                </td>
                <td>
                    <form action="{{ route('sub_category.destroy', $sub_category->id) }}" method="POST">
                        <a href="{{ route('sub_category.show', $sub_category->id) }}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>

                        <a href="{{ route('sub_category.edit', $sub_category->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>

                        </a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>

                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $sub_categories->links() !!}

@endsection
