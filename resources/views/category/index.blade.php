@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Categories</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('category.create') }}" title="Create a category"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Type</th>
            <th>Code</th>
            <th>Is Active</th>
            <th>Created At</th>
            <th>Sub-Categories of category</th>
            <th>Products of category</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($categories as $category)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ $category->type }}</td>
                <td>{{ $category->code }}</td>
                <td>{{ $category->is_active }}</td>
                <td>{{ $category->created_at->format('jS M Y') }}</td>
                <td align="center">
                    <a href="{{ 'category/sub_categories/'.$category->id }}" title="show">
                        <i class="fas fa-align-justify  fa-lg"></i>
                    </a>
                </td>
                <td align="center">
                    <a href="{{ 'category/products/'.$category->id }}" title="show">
                        <i class="fas fa-align-justify  fa-lg"></i>
                    </a>
                </td>
                <td>
                    <form action="{{ route('category.destroy', $category->id) }}" method="POST">

                        <a href="{{ route('category.show', $category->id) }}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>

                        <a href="{{ route('category.edit', $category->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>

                        </a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>

                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $categories->links() !!}

@endsection
