<?php

namespace Database\Seeders;

use App\Models\MeasurementType;
use App\Models\MeasurementUnit;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('measurement_types')->delete();
        DB::statement('ALTER TABLE measurement_types AUTO_INCREMENT = 1;');
        DB::table('measurement_units')->delete();
        DB::statement('ALTER TABLE measurement_units AUTO_INCREMENT = 1;');
        $types = [
            'VOLUMN'=>[
                [
                    'name' => 'LITER',
                    'is_default' => 1,
                    'base_unit_quantity' => 1,
                    'singular_short_code' => 'LTR',
                    'singular_code' => 'LITER',
                    'plural_short_code' => 'LTRS',
                    'plural_code' => 'LITERS'
                ]
            ],
            'WEIGHT'=>[
                [
                    'name' => 'KILOGRAM',
                    'is_default' => 1,
                    'base_unit_quantity' => 1,
                    'singular_short_code' => 'KG',
                    'singular_code' => 'KILOGRAM',
                    'plural_short_code' => 'KGS',
                    'plural_code' => 'KILOGRAMS'
                ]
            ],
            'LENGTH'=>[
                [
                    'name' => 'METER',
                    'is_default' => 1,
                    'base_unit_quantity' => 1,
                    'singular_short_code' => 'MTR',
                    'singular_code' => 'METER',
                    'plural_short_code' => 'MTRS',
                    'plural_code' => 'METERS'
                ]
            ],
            'TIME'=>[
                [
                    'name' => 'MINUTE',
                    'is_default' => 1,
                    'base_unit_quantity' => 1,
                    'singular_short_code' => 'MIN',
                    'singular_code' => 'MINUTE',
                    'plural_short_code' => 'MINS',
                    'plural_code' => 'MINUTES'
                ]
            ]
        ];
        foreach($types as $type => $units){
            $measurement_type = MeasurementType::create([
                'name' => $type
            ]);
            foreach ($units as $key => $unit) {
                $unit['measurement_id'] = $measurement_type->id;
                MeasurementUnit::create($unit);
            }
        }
    }
}
