<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeasurementUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurement_units', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('measurement_id')->index('measurement_id');
            $table->string('name', 40);
            $table->boolean('is_default')->default(false);
            $table->float('base_unit_quantity', 10, 0)->nullable();
            $table->string('singular_short_code', 40);
            $table->string('singular_code', 40);
            $table->string('plural_short_code', 40);
            $table->string('plural_code', 40);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurement_units');
    }
}
