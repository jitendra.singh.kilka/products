<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMeasurementUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('measurement_units', function (Blueprint $table) {
            $table->foreign(['measurement_id'], 'measurement_units_ibfk_1')->references(['id'])->on('measurement_types')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('measurement_units', function (Blueprint $table) {
            $table->dropForeign('measurement_units_ibfk_1');
        });
    }
}
