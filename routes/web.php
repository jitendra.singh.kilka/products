<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SubCategoryController;


Route::resource('/', CategoryController::class);

Route::resource('category', CategoryController::class);
Route::resource('sub_category', SubCategoryController::class);
Route::resource('product', ProductController::class);

Route::get('category/products/{category_id}', [ProductController::class , 'category_products']);
Route::get('category/sub_categories/{category_id}', [SubCategoryController::class , 'category_sub_categories']);
Route::get('sub_category/products/{sub_category_id}', [ProductController::class , 'sub_category_products']);


