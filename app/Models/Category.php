<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	use SoftDeletes;
	protected $table = 'categories';

	protected $casts = [
		'is_active' => 'bool'
	];

	protected $fillable = [
		'name',
		'type',
		'is_active',
		'code'
	];

	public function sub_categories()
	{
		return $this->hasMany(SubCategory::class);
	}
}
