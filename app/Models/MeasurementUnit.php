<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeasurementUnit extends Model
{
	use SoftDeletes;
	protected $table = 'measurement_units';

	protected $casts = [
		'measurement_id' => 'int',
		'is_default' => 'bool',
		'base_unit_quantity' => 'float'
	];

	protected $fillable = [
		'measurement_id',
		'name',
		'is_default',
		'base_unit_quantity',
		'singular_short_code',
		'singular_code',
		'plural_short_code',
		'plural_code'
	];

	public function measurement_type()
	{
		return $this->belongsTo(MeasurementType::class, 'measurement_id');
	}
}
