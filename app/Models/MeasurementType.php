<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeasurementType extends Model
{
	use SoftDeletes;
	protected $table = 'measurement_types';

	protected $fillable = [
		'name'
	];

	public function measurement_units()
	{
		return $this->hasMany(MeasurementUnit::class, 'measurement_id');
	}
}
