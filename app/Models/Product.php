<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;
	protected $table = 'products';

	protected $casts = [
		'sub_category_id' => 'int',
		'is_active' => 'bool',
		'measurement_unit_id' => 'int'
	];

	protected $fillable = [
		'sub_category_id',
		'name',
		'code',
		'description',
		'is_active',
		'tags',
		'measurement_unit_id',
		'hsn_code'
	];

	public function sub_category()
	{
		return $this->belongsTo(SubCategory::class);
	}
    public function measurement_unit()
	{
		return $this->belongsTo(MeasurementUnit::class);
	}
}
