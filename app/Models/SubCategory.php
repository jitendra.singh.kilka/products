<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
	use SoftDeletes;
	protected $table = 'sub_categories';

	protected $casts = [
		'category_id' => 'int',
		'is_active' => 'bool'
	];

	protected $fillable = [
		'category_id',
		'name',
		'code',
		'is_active'
	];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function products()
	{
		return $this->hasMany(Product::class);
	}
}
