<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::latest()->paginate(5);

        return view('category.index', compact('categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function create()
    {
        return view('category.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => [
                'required',
                'unique:categories,name'
            ],
            'type' => 'required',
            'code' => [
                'required',
                'unique:categories,code'
            ]
        ]);

        Category::create($request->all());

        return redirect()->route('category.index')
            ->with('success', 'Category created successfully.');
    }

    public function show(Category $category)
    {
        return view('category.show', compact('category'));
    }


    public function edit(Category $category)
    {
        return view('category.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => [
                'required',
                function ($a, $v, $f) use ($category) {
                    if (Category::where('id', '!=', $category->id)->where('name', '=', $v)->exists()) {
                        $f("Name Aleady taken");
                    }
                }
            ],
            'type' => 'required',
            'code' => [
                'required',
                function ($a, $v, $f) use ($category) {
                    if (Category::where('id', '!=', $category->id)->where('code', '=', $v)->exists()) {
                        $f("Code Aleady taken");
                    }
                }
            ]
        ]);
        $category->update($request->all());

        return redirect()->route('category.index')
            ->with('success', 'Category updated successfully');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('category.index')
            ->with('success', 'Category deleted successfully');
    }
}
