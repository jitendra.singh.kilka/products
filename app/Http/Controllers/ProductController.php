<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\MeasurementUnit;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::with(['sub_category.category', 'measurement_unit.measurement_type'])->latest()->paginate(5);

        return view('product.index', compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function category_products(Request $request)
    {
        $category_id = $request->route('category_id');
        $products = Product::with(['sub_category.category', 'measurement_unit.measurement_type'])
            ->whereHas('sub_category', function ($q) use ($category_id) {
                $q->where('category_id', '=', $category_id);
            })->latest()->paginate(5);

        return view('product.index', compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function sub_category_products(Request $request)
    {
        $sub_category_id = $request->route('sub_category_id');
        $products = Product::with(['sub_category.category', 'measurement_unit.measurement_type'])
            ->where('sub_category_id', '=', $sub_category_id)->latest()->paginate(5);

        return view('product.index', compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function create()
    {
        $result['sub_categories'] = SubCategory::where('is_active', '=', true)->get();
        $result['measurement_units'] = MeasurementUnit::with(['measurement_type'])->get();
        return view('product.create', compact('result'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => [
                'required',
                'unique:products,name'
            ],
            'code' => [
                'required',
                'unique:products,code'
            ],
            'sub_category_id' => 'required',
            'measurement_unit_id' => 'required'
        ]);

        Product::create($request->all());

        return redirect()->route('product.index')
            ->with('success', 'Product created successfully.');
    }

    public function show(Product $product)
    {
        return view('product.show', compact('product'));
    }


    public function edit(Product $product)
    {
        $result['sub_categories'] = SubCategory::where('is_active', '=', true)->get();
        $result['measurement_units'] = MeasurementUnit::with(['measurement_type'])->get();
        $result['product'] = $product;
        return view('product.edit', compact('result'));
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => [
                'required',
                function ($a, $v, $f) use ($product) {
                    if (Product::where('id', '!=', $product->id)->where('name', '=', $v)->exists()) {
                        $f("Name Aleady taken");
                    }
                }
            ],
            'code' => [
                'required',
                function ($a, $v, $f) use ($product) {
                    if (Product::where('id', '!=', $product->id)->where('code', '=', $v)->exists()) {
                        $f("Code Aleady taken");
                    }
                }
            ],
            'sub_category_id' => 'required',
            'measurement_unit_id' => 'required'
        ]);
        $product->update($request->all());

        return redirect()->route('product.index')
            ->with('success', 'Product updated successfully');
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('product.index')
            ->with('success', 'Product deleted successfully');
    }
}
