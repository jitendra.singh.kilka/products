<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{

    public function index()
    {
        $sub_categories = SubCategory::with(['category'])->latest()->paginate(5);

        return view('sub_category.index', compact('sub_categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function category_sub_categories(Request $request)
    {
        $category_id = $request->route('category_id');
        $sub_categories = SubCategory::with(['category'])
            ->where('category_id', '=', $category_id)->latest()->paginate(5);

        return view('sub_category.index', compact('sub_categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function create()
    {
        $categories = Category::where('is_active', '=', true)->get();
        return view('sub_category.create', compact('categories'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => [
                'required',
                'unique:sub_categories,name'
            ],
            'code' => [
                'required',
                'unique:sub_categories,code'
            ],
            'category_id' => 'required'
        ]);

        SubCategory::create($request->all());

        return redirect()->route('sub_category.index')
            ->with('success', 'Sub Category created successfully.');
    }

    public function show(SubCategory $sub_category)
    {
        return view('sub_category.show', compact('sub_category'));
    }


    public function edit(SubCategory $sub_category)
    {
        $result['categories'] = Category::where('is_active', '=', true)->get();
        $result['sub_category'] = $sub_category;
        return view('sub_category.edit', compact('result'));
    }

    public function update(Request $request, SubCategory $sub_category)
    {
        $request->validate([
            'name' => [
                'required',
                function ($a, $v, $f) use ($sub_category) {
                    if (SubCategory::where('id', '!=', $sub_category->id)->where('name', '=', $v)->exists()) {
                        $f("Name Aleady taken");
                    }
                }
            ],
            'category_id' => 'required',
            'code' => [
                'required',
                function ($a, $v, $f) use ($sub_category) {
                    if (SubCategory::where('id', '!=', $sub_category->id)->where('code', '=', $v)->exists()) {
                        $f("Code Aleady taken");
                    }
                }
            ]
        ]);
        $sub_category->update($request->all());

        return redirect()->route('sub_category.index')
            ->with('success', 'Sub Category updated successfully');
    }

    public function destroy(SubCategory $sub_category)
    {
        $sub_category->delete();

        return redirect()->route('sub_category.index')
            ->with('success', 'Sub Category deleted successfully');
    }
}
